import styled from 'styled-components';

export const Logo = styled.div`
    height: 60px;
`;

export const Header = styled.div`
    height: 60px;
    width: 100%;
    -webkit-box-shadow: 0px -1px 14px -2px rgba(0,0,0,0.29);
    display: flex;
    justify-content: end;
    align-items: center;
    padding: 0 25px 0 25px;
    background: #ffffff;
`;
