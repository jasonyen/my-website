import React from 'react';
import Crumb from '@/components/Crumb/Crumb';

const skillSet = ["React", "JavaScript", "CSS", "TypeScript", "Docker", "AWS EC2", "AWS ECR", "CI/CD Basics", "Git", "SQL Basics", "Django Basics"];
const About = () => {
    return (
        <div className="about-body">
            <div className="content">
                <span className="title">Get to know me</span>
                <div className="description">
                    <p>
                        Hello, I'm an experienced Front-end Developer with a passion for user-centric design.
                        I specialize in crafting seamless user experiences and high-quality web applications.</p>
                    <p>
                        I have a strong understanding of HTML, CSS, and JavaScript, as well as experience with front-end framework such as React.
                        I'm a problem solver at heart and take pride in finding solutions that balance aesthetics with functionality.
                    </p>
                    <p>
                        When I'm not coding, I enjoy playing the guitar and singing a song, which I find helps me stay inspired and creative. Let's work together to build something amazing!
                    </p>
                </div>
            </div>
            <div className="content">
                <span className="title">My skills</span>
                <div className="skills">
                    {
                        skillSet.map((skill, index) => (
                            <Crumb key={index} title={skill} />
                        ))
                    }
                </div>
            </div>
        </div>
    );
}

export default About;