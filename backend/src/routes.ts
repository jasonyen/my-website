import express, { RequestHandler } from 'express';
import * as AuthController from './controllers/AuthController';
import { isAuthorized } from './middlewares/Passport';
const routers = express.Router();

interface IRouteItem {
    path: string;
    method: 'get' | 'post' | 'put' | 'delete';
    middlewares: RequestHandler[];
};

const PREFIX = {
    AUTHORIZE: '/authorize',
    POST: '/post',
    WEB_CRAWLER: '/crawler',
    SCHEDULER: '/scheduler',
    CITYSETTING: '/citysetting',
    COUNTRY: '/country',
    CITY: '/city',
    ITEM: '/item'
};

export const AppRoutes: IRouteItem[] = [
    {
        path: `/loginCallback`,
        method: 'post',
        middlewares: [
            AuthController.loginCallback,
        ],
    },
    {
        path: `/logout`,
        method: 'post',
        middlewares: [
            AuthController.logout
        ]
    }
];
