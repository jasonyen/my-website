import { Request, Response, NextFunction } from 'express';
import { Secret, verify } from 'jsonwebtoken';
import { WEB_SAMPLE_JWT_SECRET } from '../config/index';

interface IJwtDecode {
    userId: String,
    name: String,
    email: String
};

interface IJwtRequest extends Request {
    jwtDecode?: IJwtDecode
};

export const isAuthorized = (req: Request, res: Response, next: NextFunction) => {
    const token = req.cookies.accessToken;
    if (token) {
        try {
            const decode = verify(token, WEB_SAMPLE_JWT_SECRET) as any;
            if (decode.userId) {
                next();
            } else {
                return res.status(500).json({ message: 'INVALID_USER' });
                // throw new Error('INVALID_USER 1');
            }
        } catch (error) {
            return res.status(500).json({ message: 'INVALID_USER' });
            // throw new Error('INVALID_USER 2');
        }
    } else {
        return res.status(500).json({ message: 'INVALID_USER' });
        // throw new Error('INVALID_USER 3');
    }
};